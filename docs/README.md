# Componente Personalizado
Para la elavoración de un componente personalizado, primero es escoger el tipo de componente que vamos a utilizar para que extienda ese tipo de componente. Por ejemplo si queremos que sea un JButton tiene que extender de JButton. Además que para que sea persistente tiene que implementar la clase Serializable, tanto el componente, como las clases que necesita para su funcionamiento.

A continuación creamos de manera privada las propiedades que le queremos añadir a nuestro componente. Para las propiedades del componente tenemos que crear los getters y los setters de manera automática, por NetBeans, para que los reconozca y no nos falle además de un constructor vacio. Con estes pasos ya podríamos añadir a la paleta de Beans nuestro componente, claro esta, despues de compilarlo. Para añadirlo, hacemos click derecho sobre el componente a continuación Herramientas->Add to Palette y seleccionamos la categoria Beans para organizarlos.

En el caso de que nuestra propiedad una propiedad simple de las siguentes: String, int, float, boolean, NetBeans ya mostraría editores personalizados. Al acceder a una propiedad simple distinta a las anteriores, tendremos que darle funcionalidad nosotros mismos y para ello necesitamos saber que metodo o metodos tendremos que modificar. Una vez modificado eses metodos ya podríamos comprobar si nuestro componente funciona despues de compilarlo y añadirlo al JFrame.

Para una propiedad compuesta vamos a crear una clase nueva en donde tendríamos instanciadas las propiedades que necesitaramos para la propiedad compuesta. Esta clase como se dijo anteriormente tiene que implemetar Serializable y tenemos que generar los getters y setters ademas del constructor.

A contrinuación cereamos el panel personalizado del editor. Para esto creamos un nuevo JPanel en el paquete donde estamos construyendo el componente. Este panel lo vamos a tener que diseñar para que nos permita seleccionar los valores de las propiedades. Solo es necesario crear el panel ya que el resto de botones los genera NetBeans. Ademas el panel debe implementar un metodo público, el cual, devuleva el valor de la propiedad compuesta, es decir, un objeto del tipo de nuestra clase donde tenemos las propiedades. Para poder continuar deberíamos modificar los metodos modificados anteriormente en la clase principal para que funcionen con los metodos que acabamos de crear.

Ahora tenemos que enlazar el panel con el editor personalizado. Este enlace se hace a traves de una clase de extiende de PropertyEditorSupport. Para esto creamos una nueva clase en el paquete que extiende de PropertyEditorSupport y tenemos que redifinir una seria de metodos como son:

    -supportsCustomEditor: este metodo comprueba si hay un editor personalizado para la propiedad.
        Para nuestro caso tiene que devolver true ya que si que tenemos un editor.

    -getCustomEditor: este método debe devolver el panel del editor personalizado. 
        Esta clase tiene una propiedad con el panel personalizado e inicializado. El método devolverá esta variable.

    -getJavaInitializationString: devuelve un String que servirá para inicializar el valor de la propiedad compuesta con los valores seleccionados en el panel.

    -getValue: una vez mostrado el panel y pulsado el botón aceptar, devuelve el valor de la propiedad compleja.

Nos falta unir el PropertyEditorSuppor con la propiedad, esto sucede porque no esta vinculado de ninguna manera. Esto se hace a través del BeanInfo que crea NetBeans automáticamente. Esto se hace pulsando click derecho sobre el componete ->Bean Info Editor y creamos en Bean Info. Accedemos a la vista del diseño de la clase que se creo y bucamos la propiedad personalizada, a continuación, en "Clase del editod de propiedades" hay que indicar la clase del panel con la ruta que incluya tambien el paquete. Al realizar estes paso el componente ya estaria finalizado.

